import java.util.Scanner;


public class Game {
	private Board board;
	private Player x;
	private Player o;
	
	
	public Game() {
		o =new Player ('O');
		x =new Player ('X'); 
		
	}
	
	
	public void play() {
		while(true) {
			playOne();
		}
	}
	
	
	
	
	
	public void playOne() {
		board = new Board(x,o); 
		printWelcom();
		while(true) {
			printTable();
			printTurn();
			input();
			if(board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		printTable(); 
		try {
			printWinner();
		}catch (Exception e) {
			System.out.println("Draw!!");
		}
		printStat();
		//printBye();
	}
	
	private void printBye() {
		System.out.println("Bye Bye!!");
	}
	
	
	private void printStat() {
		System.out.println(x.getName()+" W,D,L: "+x.getWin()+","+x.getDraw()+","+x.getLose());
		System.out.println(o.getName()+" W,D,L: "+o.getWin()+","+o.getDraw()+","+o.getLose());
	}
	
	private void printWinner() {
		Player player =board.getWinner();
		System.out.println(player.getName()+" Win");
	}
	
	private void printWelcom() {
		System.out.println("Welcome to OX Game <3");
	}
	
	private void printTable() {
		char[][] Table =board.getTable();
		
		for(int i =0; i<Table.length;i++) {
			for(int j= 0;j<Table[i].length;j++) {
				System.out.print(" "+Table[i][j]);
			}	System.out.println();
		}
		
	}
	
	private void printTurn() {
		System.out.println(board.getCurrentPlayer().getName()+" TURN");
	}
	
	private void input() {
		
		Scanner kb = new Scanner(System.in);
		while(true) {
			try {
				System.out.println("Please input Row Col : ");
				String  input =kb.nextLine();
				String [] str = input.split(" ");
				if(str.length!=2) {
					System.out.println("Please input Row Col[1-3] (ex: 1 2)");
					continue;
				}
				int row =Integer.parseInt(str[0])-1;
				int col =Integer.parseInt(str[1])-1;
				if(board.setTable(row,col)==false) {
					System.out.println("Table is not empty");
					continue;
				}
				break;
			}catch(Exception e) {
				System.out.println("Please input Row Col[1-3] (ex: 1 2)");
				continue;
			}
		}
		
			
		
	}
	
}
